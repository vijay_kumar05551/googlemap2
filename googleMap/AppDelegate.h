//
//  AppDelegate.h
//  googleMap
//
//  Created by Cli16 on 11/3/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

