//
//  ViewController.m
//  googleMap
//
//  Created by Cli16 on 11/3/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import "ViewController.h"
#import <MapKit/MapKit.h>
#import <GoogleMaps/GoogleMaps.h>
@interface ViewController () <MKMapViewDelegate>
{
    CLLocationManager *manager;
}
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet MKMapView *mapViewer;
@property (strong, nonatomic) IBOutlet UIView *googleViewer;

@end

@implementation ViewController
@synthesize searchBar;
@synthesize mapViewer;
@synthesize googleViewer;
GMSMapView *mapView;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    manager=[[CLLocationManager alloc]init];
    manager.delegate=self;
    manager.desiredAccuracy=kCLLocationAccuracyBest;
    [manager requestAlwaysAuthorization];
    
    self.mapViewer.delegate=self;
}
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800);
    [self.mapViewer setRegion:[self.mapViewer regionThatFits:region] animated:YES];
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate=userLocation.coordinate;
    point.title=@"where am i";
    point.subtitle=@"i am here";
    [self.mapViewer addAnnotation:point];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar;                     // called when keyboard search button pressed
{
    NSString *address;
    address=searchBar.text;
    NSString *urlString=[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?address=%@&key=AIzaSyCNNz8-pZqU2ocDSR21fVB_7KYhHBJaUQM",address];
    NSString *url=[NSURL URLWithString:urlString];
    NSData *data=[NSData dataWithContentsOfFile:url];
    NSDictionary *jSon=[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    NSLog(@"%@",jSon);
    NSArray *resultArray=jSon[@"result"];
    for (int i=0; i<resultArray.count;i++) {
        NSDictionary * addressCompontDict =resultArray[i];
        NSLog(@"%@",resultArray);
    }
    
                         }
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
